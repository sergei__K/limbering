package limbering;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Limbering {
    public static void main(String[] arg) {
        Scanner scanner = new Scanner(System.in);
        Random random = new Random();
        System.out.println("Задайте время");
        int t = scanner.nextInt();
        System.out.println("Задайте количество заказов");
        int n = scanner.nextInt();
        int[] orders = new int[n];
        for (int i = 0; i < n; i++) {
            orders[i]=(random.nextInt(100));
        }
        System.out.print("заказы - ");
        for (int i = 0; i < n; i++) {
            System.out.print(orders[i] + " ");
        }
        System.out.println();
        Arrays.sort(orders);

        if (n<t){
            t=n;
        }

        System.out.println("Сумма выгодных заказов-" +sum(t,orders));
    }
    public static  int sum (int t,int[] orders){
        int sum=0;
        for (int i = 0; i < t; i++) {
            sum = sum + orders[i];
        }
        return sum;

    }
}